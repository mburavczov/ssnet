package ru.nutscoon.androidapp

import android.app.Application
import ru.nutscoon.androidapp.di.ApplicationComponent
import ru.nutscoon.androidapp.di.ApplicationModule
import ru.nutscoon.androidapp.di.DaggerApplicationComponent

class App : Application(){
    companion object{
        lateinit var component: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger(){
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}
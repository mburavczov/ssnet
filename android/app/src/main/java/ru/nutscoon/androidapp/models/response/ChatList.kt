package ru.nutscoon.androidapp.models.response

import java.util.*

data class ChatList(val chats: List<ChatListItem>)
data class ChatListItem(val id: Int, val name: String, val persons: List<ChatPersonItem>, val lastMsg: String?, val date: Date?, val preview: String)
data class ChatPersonItem(val personId: Int, val name: String)
package ru.nutscoon.androidapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import ru.nutscoon.androidapp.databinding.FragmentHomeBinding
import ru.nutscoon.androidapp.ui.CircleTransform


class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        viewModel.errorState.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.profileInfoContainer.visibility = View.GONE
                binding.tvProfileErr.visibility = View.VISIBLE
            }
        })
        viewModel.profileInfo.observe(viewLifecycleOwner, Observer {
            val name = "${it.name} ${it.surname}"
            binding.tvProfileName.text = name
            binding.tvProfileEmail.text = it.login
            binding.tvProfilePhone.text = it.phone
            if (!it.avatarPath.isNullOrEmpty()) {
                Picasso.get().load(it.avatarPath).transform(CircleTransform()).into(binding.profileImage);
            }
        })
        return root
    }

    override fun onStart() {
        super.onStart()
        viewModel.ready()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
package ru.nutscoon.androidapp.di

import android.content.Context
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.nutscoon.androidapp.services.ISocialNetworkApiService
import ru.nutscoon.androidapp.services.SharedPreference
import javax.inject.Singleton


@Module
class ApplicationModule(private val context: Context) {

    @Provides
    @Singleton
    fun getApiService(): ISocialNetworkApiService {
        return getRetrofit("https://phoenix-art-api.herokuapp.com/api/")
            .create(ISocialNetworkApiService::class.java)
    }

    @Provides
    @Singleton
    fun getSharedPreference(): SharedPreference {
        return SharedPreference(context)
    }

    var client = OkHttpClient.Builder().addInterceptor { chain ->
        val token = getSharedPreference().getValueString("API_TOKEN")
        val newRequest: Request = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer $token")
            .build()
        chain.proceed(newRequest)
    }.build()

    private fun getRetrofit(url: String): Retrofit {
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
        return Retrofit.Builder()
            .client(client)
            .baseUrl(url)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
}
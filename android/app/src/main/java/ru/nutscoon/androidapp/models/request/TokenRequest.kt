package ru.nutscoon.androidapp.models.request

data class TokenRequest(val login: String, val pass: String)

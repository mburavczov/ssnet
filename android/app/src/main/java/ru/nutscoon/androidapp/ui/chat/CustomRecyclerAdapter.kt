package ru.nutscoon.androidapp.ui.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ru.nutscoon.androidapp.databinding.ChatItemBinding
import ru.nutscoon.androidapp.models.response.ChatListItem
import ru.nutscoon.androidapp.ui.CircleTransform


class RvAdapter(
    var chats: List<ChatListItem>,
) : RecyclerView.Adapter<RvAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ChatItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ChatItemBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            with(chats[position]){

                binding.tvChatName.text = this.name
                binding.tvChatMsg.text = this.lastMsg
                binding.tvChatDate.text = "Отправлено ${this.date}"
                if (!this.preview.isNullOrEmpty()){
                    Picasso.get().load(this.preview).transform(CircleTransform()).into(binding.ivChatAvatar);
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return chats.size
    }
}


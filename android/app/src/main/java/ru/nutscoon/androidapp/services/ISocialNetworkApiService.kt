package ru.nutscoon.androidapp.services

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import ru.nutscoon.androidapp.models.request.RegisterRequest
import ru.nutscoon.androidapp.models.request.TokenRequest
import ru.nutscoon.androidapp.models.response.ChatList
import ru.nutscoon.androidapp.models.response.ProfileInfo
import ru.nutscoon.androidapp.models.response.TokenResponse


interface ISocialNetworkApiService {
    @POST("account/token")
    fun token(@Body model: TokenRequest): Call<TokenResponse?>
    @POST("account/register")
    fun register(@Body model: RegisterRequest): Call<TokenResponse?>
    @GET("profile/info")
    fun profileInfo(): Call<ProfileInfo?>
    @GET("chat/chats")
    fun chats(): Call<ChatList>
}
package ru.nutscoon.androidapp.ui.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import ru.nutscoon.androidapp.databinding.FragmentChatBinding


class ChatFragment : Fragment() {

    private lateinit var viewModel: ChatViewModel
    private var _binding: FragmentChatBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(ChatViewModel::class.java)

        _binding = FragmentChatBinding.inflate(inflater, container, false)
        val root: View = binding.root

        viewModel.errorState.observe(viewLifecycleOwner, {
            if (it) {
                binding.rvChats.visibility = View.GONE
                binding.tvChatErr.visibility = View.VISIBLE
            }
        })
        viewModel.chats.observe(viewLifecycleOwner, {
            if (it.chats.isEmpty()){
                binding.rvChats.visibility = View.GONE
                binding.tvChatEmpty.visibility = View.VISIBLE
            } else {
                binding.rvChats.layoutManager = LinearLayoutManager(this.context)
                binding.rvChats.adapter = RvAdapter(it.chats)
            }
        })
        return root
    }

    override fun onStart() {
        super.onStart()
        viewModel.ready()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
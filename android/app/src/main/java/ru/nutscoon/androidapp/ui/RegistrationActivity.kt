package ru.nutscoon.androidapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import ru.nutscoon.androidapp.R
import ru.nutscoon.androidapp.databinding.ActivityRegistrationBinding
import ru.nutscoon.androidapp.viewModels.RegistrationViewModel

class RegistrationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegistrationBinding

    private lateinit var viewModel: RegistrationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
        setContentView(R.layout.activity_login)

        binding = ActivityRegistrationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val loginBtn: Button = binding.btnReg
        loginBtn.setOnClickListener {
            viewModel.registration(binding.etEmailR.text.toString(), binding.etPasswordR.text.toString(), binding.etNameR.text.toString())
        }
    }

    private fun setupViewModel(){
        viewModel = ViewModelProviders.of(this).get(RegistrationViewModel::class.java)
        viewModel.regResult.observe(this, {
            if (!it) {
                binding.tvError.visibility = View.VISIBLE
            } else {
                val intent = Intent(this, MainActivity2::class.java)
                startActivity(intent)
                finish()
            }
        })
    }
}
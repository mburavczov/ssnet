package ru.nutscoon.androidapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import ru.nutscoon.androidapp.R
import ru.nutscoon.androidapp.databinding.ActivityLoginBinding
import ru.nutscoon.androidapp.viewModels.LoginViewModel


class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
        setContentView(R.layout.activity_login)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {
            viewModel.login(binding.etEmail.text.toString(), binding.etPassword.text.toString())
        }
        binding.btnReg.setOnClickListener {
            val intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setupViewModel(){
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        viewModel.loginState.observe(this, {
            if (!it) {
                binding.tvWrongPass.visibility = View.VISIBLE
            } else {
                val intent = Intent(this, MainActivity2::class.java)
                startActivity(intent)
            }
        })
    }
}
package ru.nutscoon.androidapp.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ru.nutscoon.androidapp.App
import ru.nutscoon.androidapp.models.response.ProfileInfo
import ru.nutscoon.androidapp.services.ISocialNetworkApiService
import javax.inject.Inject

class HomeViewModel : ViewModel() {

    @Inject
    lateinit var apiService: ISocialNetworkApiService

    var profileInfo: MutableLiveData<ProfileInfo> = MutableLiveData()
    var errorState: MutableLiveData<Boolean> = MutableLiveData()

    init {
        App.component.inject(this)
    }

    fun ready() {
        val handler = Dispatchers.IO + CoroutineExceptionHandler { _, exception ->
            GlobalScope.launch(Dispatchers.Main) {
                errorState.value = true
            }
        }

        CoroutineScope(handler).launch {
            val response = apiService.profileInfo().execute()
            if (response.isSuccessful) {
                val info = response.body()!!

                withContext(Dispatchers.Main){
                    profileInfo.value = info
                    errorState.value = false
                }
            } else {
                withContext(Dispatchers.Main){
                    errorState.value = true
                }
            }
        }
    }
}
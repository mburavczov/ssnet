package ru.nutscoon.androidapp.models.response

data class TokenResponse(val token: String)
package ru.nutscoon.androidapp.di

import dagger.Component
import ru.nutscoon.androidapp.services.ISocialNetworkApiService
import ru.nutscoon.androidapp.services.SharedPreference
import ru.nutscoon.androidapp.ui.chat.ChatViewModel
import ru.nutscoon.androidapp.ui.home.HomeViewModel
import ru.nutscoon.androidapp.viewModels.LoginViewModel
import ru.nutscoon.androidapp.viewModels.RegistrationViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun getApiService(): ISocialNetworkApiService
    fun getSharedPreference(): SharedPreference
    fun inject(viewModel: LoginViewModel)
    fun inject(viewModel: RegistrationViewModel)
    fun inject(viewModel: HomeViewModel)
    fun inject(viewModel: ChatViewModel)
}
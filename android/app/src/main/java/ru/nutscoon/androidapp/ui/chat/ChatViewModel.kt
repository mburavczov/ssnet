package ru.nutscoon.androidapp.ui.chat

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ru.nutscoon.androidapp.App
import ru.nutscoon.androidapp.models.response.ChatList
import ru.nutscoon.androidapp.services.ISocialNetworkApiService
import javax.inject.Inject

class ChatViewModel : ViewModel() {

    @Inject
    lateinit var apiService: ISocialNetworkApiService

    var chats: MutableLiveData<ChatList> = MutableLiveData()
    var errorState: MutableLiveData<Boolean> = MutableLiveData()

    init {
        App.component.inject(this)
    }

    fun ready() {
        val handler = Dispatchers.IO + CoroutineExceptionHandler { _, exception ->
            GlobalScope.launch(Dispatchers.Main) {
                errorState.value = true
            }
        }

        CoroutineScope(handler).launch {
            val response = apiService.chats().execute()
            if (response.isSuccessful) {
                val info = response.body()!!

                withContext(Dispatchers.Main){
                    chats.value = info
                    errorState.value = false
                }
            } else {
                withContext(Dispatchers.Main){
                    errorState.value = true
                }
            }
        }
    }
}
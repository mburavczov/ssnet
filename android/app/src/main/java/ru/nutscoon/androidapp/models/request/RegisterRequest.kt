package ru.nutscoon.androidapp.models.request

data class RegisterRequest(val login: String, val pass: String, val name: String)
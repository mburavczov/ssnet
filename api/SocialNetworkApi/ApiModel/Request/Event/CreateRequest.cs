﻿using System;

namespace SocialNetworkApi.ApiModel.Request.Schedule
{
    public class CreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
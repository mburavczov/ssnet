﻿namespace SocialNetworkApi.ApiModel.Request.Schedule
{
    public class CancelEventRequest
    {
        public int EventId { get; set; }
    }
}
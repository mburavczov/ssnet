﻿namespace SocialNetworkApi.ApiModel.Request.Chat
{
    public class SendMessageDto
    {
        public int ChatId { get; set; }
        public string Text { get; set; }
    }
}
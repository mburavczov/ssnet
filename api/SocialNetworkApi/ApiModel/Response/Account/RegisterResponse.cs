﻿namespace SocialNetworkApi.ApiModel.Response.Account
{
    public class RegisterResponse
    {
        public string Token { get; set; }
    }
}
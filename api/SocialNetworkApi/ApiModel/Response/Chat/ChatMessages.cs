﻿using System;
using System.Collections.Generic;

namespace SocialNetworkApi.ApiModel.Response.Chat
{
    public class ChatMessages
    {
        public List<MessageItem> Messages { get; set; }
    }

    public class MessageItem
    {
        public string Text { get; set; }
        public int SenderId { get; set; }
        public string SenderName { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
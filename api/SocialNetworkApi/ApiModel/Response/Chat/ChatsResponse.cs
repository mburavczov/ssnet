﻿using System;
using System.Collections.Generic;

namespace SocialNetworkApi.ApiModel.Response.Chat
{
    public class ChatsResponse
    {
        public List<ChatItem> Chats { get; set; }
    }

    public class ChatItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastMsg { get; set; }
        public DateTime? Date { get; set; }
        public string Preview { get; set; }
        public List<ChatPersonItem> Persons { get; set; }
    }

    public class ChatPersonItem
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
    }
}
﻿namespace SocialNetworkApi.ApiModel.Response.Chat
{
    public class CreateChatResponse
    {
        public int ChatId { get; set; }
    }
}
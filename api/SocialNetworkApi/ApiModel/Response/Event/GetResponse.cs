﻿using System;

namespace SocialNetworkApi.ApiModel.Response.Event
{
    public class GetResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool Canceled { get; set; }
    }
}
﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkApi.ApiModel.Request.Schedule;
using SocialNetworkApi.ApiModel.Response.Event;
using SocialNetworkApi.Entity;
using SocialNetworkApi.Repository;

namespace SocialNetworkApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EventController : Controller
    {
        private readonly EventRepository _eventRepository;

        
        public EventController(EventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }


        /// <summary>
        /// Create new event
        /// </summary>
        /// <response code="200">Ok</response>
        [HttpPost("Create")]
        [Authorize(Roles = "admin")]
        public IActionResult Create(CreateRequest model)
        {
            _eventRepository.Add(new Event
            {
                Name = model.Name,
                Description = model.Description,
                Start = model.Start,
                End = model.End,
            });

            return Ok();
        }

        /// <summary>
        /// Cancel event
        /// </summary>
        /// <response code="200">Ok</response>
        [HttpPost("CancelEvent")]
        [Authorize(Roles = "admin")]
        public IActionResult CancelEvent(CancelEventRequest model)
        {
            _eventRepository.CancelEvent(model.EventId);
            return Ok();
        }

        /// <summary>
        /// Get events from start to end dates. If dates don't set, returns all events
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        [HttpGet("Get")]
        [Authorize]
        public IActionResult Get(DateTime? start, DateTime? end)
        {
            return Ok(_eventRepository.GetByDates(start, end).Select(x => new GetResponse
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Start = x.Start,
                End = x.End,
                Canceled = x.Canceled
            }).ToList());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkApi.ApiModel.Request.Chat;
using SocialNetworkApi.ApiModel.Response.Chat;
using SocialNetworkApi.Entity;
using SocialNetworkApi.Repository;
using SocialNetworkApi.Settings;

namespace SocialNetworkApi.Controllers
{
    /// <summary>
    /// Methods for chats and messages
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ChatController : Controller
    {
        private readonly ChatRepository _chatRepository;
        private readonly ChatMessageRepository _chatMessageRepository;
        private readonly PersonRepository _personRepository;

        
        public ChatController(ChatRepository chatRepository, ChatMessageRepository chatMessageRepository, PersonRepository personRepository)
        {
            _chatRepository = chatRepository;
            _chatMessageRepository = chatMessageRepository;
            _personRepository = personRepository;
        }


        /// <summary>
        /// Send message to chat
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="400">Wrong chatId</response>
        [Authorize]
        [HttpPost("SendMessage")]
        public IActionResult SendMessage(SendMessageDto model)
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            if (!_chatRepository.HasByIdAndSenderId(model.ChatId, personId))
            {
                return BadRequest();
            }

            _chatMessageRepository.Add(new ChatMessage
            {
                ChatId = model.ChatId,
                SenderId = personId,
                Message = model.Text,
                CreatedAt = DateTime.Now
            });
            
            return Ok();
        }

        /// <summary>
        /// Create new chat
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="400">Wrong one or more userId</response>
        /// <returns>Chat Id</returns>
        [Authorize]
        [HttpPost("CreateChat")]
        [ProducesResponseType(typeof(CreateChatResponse), 200)]
        public IActionResult CreateChat(CreateChatDto model)
        {
            var persons = _personRepository.GetByIds(model.PersonIds);
            if (persons.Count != model.PersonIds.Length)
            {
                return BadRequest();
            }

            var chat = new Chat
            {
                Name = model.Name,
                Persons = persons
            };
            _chatRepository.Add(chat);

            return Ok(new CreateChatResponse
            {
                ChatId = chat.Id
            });
        }

        /// <summary>
        /// Get all users chats
        /// </summary>
        /// <returns>Chats</returns>
        [Authorize]
        [HttpGet("Chats")]
        [ProducesResponseType(typeof(ChatsResponse), 200)]
        public async Task<IActionResult> Chats()
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var chats = _chatRepository.GetByPersonId(personId);
            var chatItems = new List<ChatItem>();
            foreach (var chat in chats)
            {
                var lastMsg = _chatMessageRepository.GetLastByChatId(chat.Id);
                var chatItem = new ChatItem
                {
                    Id = chat.Id,
                    Name = chat.Name,
                    LastMsg = lastMsg?.Message,
                    Date = lastMsg?.CreatedAt,
                    Persons = chat.Persons?.Select(p => new ChatPersonItem
                    {
                        PersonId = p.Id,
                        Name = $"{p.Name} {p.Surname}"
                    }).ToList()
                };
                
                chatItems.Add(chatItem);
            }

            return Ok(new ChatsResponse
            {
                Chats = chatItems
            });
        }

        /// <summary>
        /// Get all messages from chat
        /// </summary>
        /// <returns>Chats</returns>
        [Authorize]
        [HttpGet("ChatMessages")]
        [ProducesResponseType(typeof(ChatMessages), 200)]
        public IActionResult ChatMessages(int chatId)
        {
            var messages = _chatMessageRepository.GetByChatId(chatId);
            var items = messages.Select(x => new MessageItem
            {
                Text = x.Message,
                CreatedAt = x.CreatedAt,
                SenderId = x.SenderId,
                SenderName = $"{x.Sender.Name} {x.Sender.Surname}"
            }).ToList();

            return Ok(new ChatMessages
            {
                Messages = items
            });
        }
    }
}
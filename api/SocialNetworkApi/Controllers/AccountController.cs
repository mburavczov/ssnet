﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SocialNetworkApi.ApiModel.Request.Account;
using SocialNetworkApi.ApiModel.Response.Account;
using SocialNetworkApi.Entity;
using SocialNetworkApi.Repository;
using SocialNetworkApi.Settings;

namespace SocialNetworkApi.Controllers
{
    /// <summary>
    /// Methods for register, authentication and authorization
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly PersonRepository _personRepository;

        public AccountController(PersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        /// <summary>
        /// Get JWT token by login and password
        /// </summary>
        /// <returns>JWT token</returns>
        /// <response code="200">Ok</response>
        /// <response code="400">Invalid username or password</response>
        [HttpPost("Token")]
        [ProducesResponseType(typeof(TokenResponse), 200)]
        public IActionResult Token([FromBody] TokenRequest data)
        {
            var person = _personRepository.Get(data.Login);
            if (person == null || !BCrypt.Net.BCrypt.Verify(data.Pass, person.Password))
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            return Json(new TokenResponse
            {
                Token = GetToken(person)
            });
        }

        /// <summary>
        /// Register new account
        /// </summary>
        /// <returns>Token</returns>
        /// <response code="200">Ok</response>
        [HttpPost("Register")]
        [ProducesResponseType(typeof(RegisterResponse), 200)]
        public IActionResult Register([FromBody] RegisterRequest data)
        {
            var person = new Person
            {
                Login = data.Login,
                Password = BCrypt.Net.BCrypt.HashPassword(data.Pass),
                Name = data.Name,
                Surname = data.Surname,
                Vk = data.Vk,
                Phone = data.Phone,
                Role = PersonRole.User,
                State = PersonState.Active,
                About = data.About
            };
            _personRepository.Add(person);

            return Ok(new RegisterResponse
            {
                Token = GetToken(person)
            });
        }

        private string GetToken(Person person)
        {
            var claims = new List<Claim>
            {
                new Claim(Constants.ClaimTypeName, person.Name),
                new Claim(Constants.ClaimTypeRole, person.Role.Name()),
                new Claim(Constants.ClaimTypeLogin, person.Login),
                new Claim(Constants.ClaimTypeUserId, person.Id.ToString())
            };
            claims.AddRange(GetIdentity(person).Claims);
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
        
        private ClaimsIdentity GetIdentity(Person person)
        {
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role.Name())
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }
    }
}
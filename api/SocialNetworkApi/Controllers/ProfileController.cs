﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Dropbox.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkApi.ApiModel.Response.Profile;
using SocialNetworkApi.Entity;
using SocialNetworkApi.Repository;
using SocialNetworkApi.Services;
using SocialNetworkApi.Settings;

namespace SocialNetworkApi.Controllers
{
    /// <summary>
    /// Methods for manage profile
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ProfileController : Controller
    {
        private readonly PersonRepository _personRepository;
        private readonly DropboxService _dropboxService;

        public ProfileController(PersonRepository personRepository, DropboxService dropboxService)
        {
            _personRepository = personRepository;
            _dropboxService = dropboxService;
        }


        /// <summary>
        /// Get profile info
        /// </summary>
        /// <param name="personId">If not specified, the current id is used</param>
        /// <response code="404">Person not found</response>
        [Authorize]
        [HttpGet("Info")]
        [ProducesResponseType(typeof(PersonItem), 200)]
        public async Task<IActionResult> Info(int? personId)
        {
            if (!personId.HasValue)
            {
                personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            }

            var person = _personRepository.GetById(personId.Value);
            if (person == null)
            {
                return NotFound();
            }

            var info = await MapPerson(person);

            return Ok(info);
        }

        /// <summary>
        /// Get all persons
        /// </summary>
        [Authorize]
        [HttpGet("Persons")]
        [ProducesResponseType(typeof(List<PersonItem>), 200)]
        public async Task<IActionResult> Persons()
        {
            var persons = _personRepository.GetAll();
            var res = new List<PersonItem>();
            foreach (var person in persons)
            {
                var info = await MapPerson(person);
                res.Add(info);
            }

            return Ok(res);
        }

        /// <summary>
        /// Update profile avatar
        /// </summary>
        /// <response code="404">Person not found</response>
        /// <response code="400">Zero or more than 1 files in request</response>
        [Authorize]
        [HttpPost("UpdateAvatar")]
        [ProducesResponseType(typeof(UpdateAvatarResponse), 200)]
        public async Task<IActionResult> UpdateAvatar()
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var person = _personRepository.GetById(personId);
            if (person == null)
            {
                return NotFound();
            }

            if (Request.Form.Files.Count != 1)
            {
                return BadRequest();
            }

            var filePath = $"/ssnet/{personId}/{Guid.NewGuid().ToString()}.jpg";
            using (var dbx = new DropboxClient(Environment.GetEnvironmentVariable("DROPBOX_TOKEN")))
            {
                using (var fs = Request.Form.Files[0].OpenReadStream())
                {
                    var bytes = ReadFully(fs);
                    await _dropboxService.Upload(bytes, filePath);
                }

                person.AvatarPath = filePath;
                _personRepository.Update(person);
                var path = await dbx.Files.GetTemporaryLinkAsync(filePath);

                return Ok(new UpdateAvatarResponse
                {
                    Link = path.Link
                });
            }
        }

        private async Task<PersonItem> MapPerson(Person person)
        {
            var info = new PersonItem
            {
                Id = person.Id,
                Login = person.Login,
                Role = person.Role.Name(),
                State = person.State.Name(),
                Name = person.Name,
                Surname = person.Surname,
                Vk = person.Vk,
                Phone = person.Phone,
                About = person.About,
            };

            if (!string.IsNullOrEmpty(person.AvatarPath))
            {
                info.AvatarPath = await _dropboxService.GetTemporaryLink(person.AvatarPath);
            }

            return info;
        }
        
        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}
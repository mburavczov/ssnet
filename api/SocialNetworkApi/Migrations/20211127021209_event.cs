﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SocialNetworkApi.Migrations
{
    public partial class @event : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatPerson_Chats_ChatsId",
                table: "ChatPerson");

            migrationBuilder.DropForeignKey(
                name: "FK_ChatPerson_Persons_PersonsId",
                table: "ChatPerson");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ChatPerson",
                table: "ChatPerson");

            migrationBuilder.RenameTable(
                name: "ChatPerson",
                newName: "PersonChat");

            migrationBuilder.RenameIndex(
                name: "IX_ChatPerson_PersonsId",
                table: "PersonChat",
                newName: "IX_PersonChat_PersonsId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Chats",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Message",
                table: "ChatMessages",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PersonChat",
                table: "PersonChat",
                columns: new[] { "ChatsId", "PersonsId" });

            migrationBuilder.CreateTable(
                name: "Schedules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Start = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    End = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedules", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_PersonChat_Chats_ChatsId",
                table: "PersonChat",
                column: "ChatsId",
                principalTable: "Chats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonChat_Persons_PersonsId",
                table: "PersonChat",
                column: "PersonsId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonChat_Chats_ChatsId",
                table: "PersonChat");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonChat_Persons_PersonsId",
                table: "PersonChat");

            migrationBuilder.DropTable(
                name: "Schedules");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PersonChat",
                table: "PersonChat");

            migrationBuilder.RenameTable(
                name: "PersonChat",
                newName: "ChatPerson");

            migrationBuilder.RenameIndex(
                name: "IX_PersonChat_PersonsId",
                table: "ChatPerson",
                newName: "IX_ChatPerson_PersonsId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Chats",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "Message",
                table: "ChatMessages",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ChatPerson",
                table: "ChatPerson",
                columns: new[] { "ChatsId", "PersonsId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ChatPerson_Chats_ChatsId",
                table: "ChatPerson",
                column: "ChatsId",
                principalTable: "Chats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ChatPerson_Persons_PersonsId",
                table: "ChatPerson",
                column: "PersonsId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

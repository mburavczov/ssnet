﻿using System;
using System.IO;
using System.Threading.Tasks;
using Dropbox.Api;
using Dropbox.Api.Files;

namespace SocialNetworkApi.Services
{
    public class DropboxService
    {
        public async Task Upload(byte[] data, string path)
        {
            using (var dbx = new DropboxClient(Environment.GetEnvironmentVariable("DROPBOX_TOKEN")))
            {
                await dbx.Files.UploadAsync(
                    path,
                    WriteMode.Overwrite.Instance,
                    body: new MemoryStream(data));
            }
        }

        public async Task<string> GetTemporaryLink(string path)
        {
            using (var dbx = new DropboxClient(Environment.GetEnvironmentVariable("DROPBOX_TOKEN")))
            {
                return (await dbx.Files.GetTemporaryLinkAsync(path)).Link;
            }
        }
    }
}
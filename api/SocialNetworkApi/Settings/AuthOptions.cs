﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace SocialNetworkApi.Settings
{
    public class AuthOptions
    {
        /// <summary>
        /// Издатель токена
        /// </summary>
        public const string ISSUER = "SocialNetworkApi";
        /// <summary>
        /// Потребитель токена
        /// </summary>
        public const string AUDIENCE = "SocialNetworkWeb";
        /// <summary>
        /// Ключ для шифрации
        /// </summary>
        const string KEY = "EFAEDCBD5D1365E626A638719C4A5";
        /// <summary>
        /// Время жизни токена
        /// </summary>
        public const int LIFETIME = 60 * 24 * 5;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
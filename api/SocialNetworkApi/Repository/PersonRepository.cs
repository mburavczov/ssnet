﻿using System.Collections.Generic;
using System.Linq;
using SocialNetworkApi.Entity;

namespace SocialNetworkApi.Repository
{
    public class PersonRepository : BaseRepository<Person>
    {
        public PersonRepository(MainContext context) : base(context)
        {
        }

        
        public Person Get(string login)
        {
            return DbSet.FirstOrDefault(x => x.Login == login);
        }
        
        public List<Person> GetAll()
        {
            return DbSet.ToList();
        }

        public void Update(Person person)
        {
            DbSet.Update(person);
            Context.SaveChanges();
        }

        public Person GetById(int id)
        {
            return DbSet.FirstOrDefault(x => x.Id == id);
        }

        public List<Person> GetByIds(int[] ids)
        {
            return DbSet.Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}
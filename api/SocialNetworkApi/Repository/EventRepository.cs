﻿using System;
using System.Collections.Generic;
using System.Linq;
using SocialNetworkApi.Entity;

namespace SocialNetworkApi.Repository
{
    public class EventRepository : BaseRepository<Event>
    {
        public EventRepository(MainContext context) : base(context)
        {
        }

        public List<Event> GetByDates(DateTime? start, DateTime? end)
        {
            return DbSet.Where(x => (!start.HasValue || x.Start == start.Value) && (!end.HasValue || x.Start == end.Value))
                .OrderBy(x => x.Start)
                .ToList();
        }

        public void CancelEvent(int eventId)
        {
            var e = DbSet.FirstOrDefault(x => x.Id == eventId);
            if (e != null)
            {
                e.Canceled = true;
                DbSet.Update(e);
                Context.SaveChanges();
            }
        }
    }
}
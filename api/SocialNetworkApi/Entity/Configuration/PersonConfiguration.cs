﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SocialNetworkApi.Entity.Configuration
{
    public class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.HasKey(x => x.Id);
            
            builder.Property(x => x.Login).IsRequired();
            builder.Property(x => x.Login).HasMaxLength(100);
            builder.HasIndex(x => x.Login).IsUnique();
            
            builder.Property(x => x.Password).IsRequired();
            builder.Property(x => x.Role).IsRequired();
            builder.Property(x => x.State).IsRequired();
            builder.Property(x => x.Name).IsRequired();
        }
    }
}
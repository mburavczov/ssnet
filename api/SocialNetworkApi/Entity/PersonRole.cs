﻿namespace SocialNetworkApi.Entity
{
    public enum PersonRole
    {
        Admin = 1,
        User = 2
    }

    public static class RoleExtension
    {
        public static string Name(this PersonRole personRole)
        {
            return personRole switch
            {
                PersonRole.Admin => "admin",
                PersonRole.User => "user",
                _ => ""
            };
        }
    }
}
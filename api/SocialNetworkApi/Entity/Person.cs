﻿using System.Collections.Generic;

namespace SocialNetworkApi.Entity
{
    public class Person
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public PersonRole Role { get; set; }
        public PersonState State { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Vk { get; set; }
        public string Phone { get; set; }
        public string About { get; set; }
        public string AvatarPath { get; set; }
        
        public virtual ICollection<Chat> Chats { get; set; }
        public virtual ICollection<ChatMessage> ChatMessages { get; set; }
    }
}
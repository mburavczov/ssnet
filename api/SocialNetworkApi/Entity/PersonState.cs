﻿namespace SocialNetworkApi.Entity
{
    public enum PersonState
    {
        Active = 1,
        Disabled = 2
    }
    
    public static class PersonStateExtension
    {
        public static string Name(this PersonState personState)
        {
            return personState switch
            {
                PersonState.Active => "active",
                PersonState.Disabled => "disabled",
                _ => ""
            };
        }
    }
}
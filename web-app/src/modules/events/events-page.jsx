import { MainLayout } from '../../components/layout';
import { useRedirectSignIn } from '../auth';
import { EventsPageMain } from './events-page-main';

export const EventsPage = () => {
  useRedirectSignIn();
  return (
    <MainLayout _sider>
      <EventsPageMain />
    </MainLayout>
  );
};

import moment from 'moment';

export const getScheduleToDay = (value, events) => {
  let list = [];

  events.forEach((event) => {
    const momentDateAsItem = moment(event.start);
    if (momentDateAsItem.format('ll') === value.format('ll'))
      list.push({
        ...event,
        start: moment(event.start).format('HH:mm'),
        end: moment(event.end).format('HH:mm'),
        startWithDate: moment(event.start).format('DD-MM-YYYY HH:mm'),
        endWithDate: moment(event.end).format('DD-MM-YYYY HH:mm'),
        status: event.cancelled ? 'error' : 'success',
      });
  });

  return list;
};

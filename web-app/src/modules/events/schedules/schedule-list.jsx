import styled from 'styled-components';
import { Typography } from 'antd';

export const ScheduleList = () => {
  return (
    <Container>
      <Typography.Title level={4}>Скоро будет</Typography.Title>
    </Container>
  );
};

const Container = styled.div`
  margin-top: 30px;
  display: flex;
  justify-content: center;
`;

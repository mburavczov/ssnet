import { Badge, Button, Calendar, Card, Modal, Typography } from 'antd';
import { LOCALE, ROLE_ADMIN } from '../../../config';
import { getScheduleToDay } from '../utils/getScheduleToDay';
import { useState } from 'react';
import { useEventCancel } from '../api/use-event-cancel';
import { useUser } from '../../../providers';

export const ScheduleCalendar = ({ events }) => {
  const [eventsForModal, setEventsForModal] = useState([]);
  const { onCancelEvent } = useEventCancel();
  const { user } = useUser();

  const onChangeCalendarDate = (value) => {
    const eventsFromDay = getScheduleToDay(value, events);
    if (eventsFromDay.length !== 0) setEventsForModal(eventsFromDay);
  };

  return (
    <>
      <Modal
        title="События дня"
        centered
        visible={eventsForModal.length > 0}
        footer={[]}
        onCancel={() => setEventsForModal([])}
      >
        {eventsForModal.map((event) => (
          <Card title={event.name}>
            <Typography.Paragraph>{event.description}</Typography.Paragraph>
            <Typography.Paragraph>Начало: {event.startWithDate}</Typography.Paragraph>
            <Typography.Paragraph>Окончание: {event.endWithDate}</Typography.Paragraph>
            {event.cancelled && <Typography.Paragraph type="danger">Отменено</Typography.Paragraph>}
            {user && user.role === ROLE_ADMIN && !event.cancelled && (
              <Button danger onClick={() => onCancelEvent(event.id)} type="link">
                Отменить
              </Button>
            )}
          </Card>
        ))}
      </Modal>
      <Calendar
        locale={LOCALE}
        dateCellRender={(value) => dateCellRender(value, events)}
        onChange={onChangeCalendarDate}
      />
    </>
  );
};

const dateCellRender = (value, events) => {
  if (!events || events.length === 0) return null;
  const newEvents = getScheduleToDay(value, events);
  if (newEvents.length === 0) return null;

  return (
    <>
      {newEvents.map((event) => (
        <Badge key={event.id} status="success" text={`${event.start} ${event.name}`} />
      ))}
    </>
  );
};

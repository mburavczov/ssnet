import { Button, DatePicker, Form, Input, Modal } from 'antd';
import { useState } from 'react';
import { useEventCreate } from './api/use-event-create';
import { LOCALE } from '../../config';

export const EventCreate = () => {
  const [isModal, setIsModal] = useState();
  const { onCreateEvent } = useEventCreate();
  const [form] = Form.useForm();

  return (
    <>
      <Button onClick={() => setIsModal(true)}>Создать событие</Button>
      <Modal
        title="Создать событие"
        centered
        visible={isModal}
        footer={[]}
        onCancel={() => setIsModal(false)}
      >
        <Form
          name="auth"
          layout={'vertical'}
          form={form}
          onFinish={async (body) => {
            setIsModal(false);
            form.resetFields();
            await onCreateEvent(body);
          }}
        >
          <Form.Item
            label="Название"
            name="name"
            rules={[
              {
                required: true,
                message: 'Данной поле должно быть заполнено',
              },
            ]}
          >
            <Input placeholder={'Введите имя'} />
          </Form.Item>
          <Form.Item
            label="Описание"
            name="description"
            rules={[
              {
                required: true,
                message: 'Данной поле должно быть заполнено',
              },
            ]}
          >
            <Input.TextArea autoSize={true} placeholder="Расскажите о событии" />
          </Form.Item>
          <Form.Item
            name="showTime"
            label="Время события"
            rules={[
              {
                type: 'array',
                required: true,
                message: 'Please select time!',
              },
            ]}
          >
            <DatePicker.RangePicker
              locale={LOCALE}
              style={{ width: '100%' }}
              showTime
              format="DD-MM-YYYY HH:mm"
            />
          </Form.Item>
          <Form.Item key="submit">
            <Button style={{ width: '100%' }} type="primary" htmlType="submit">
              Создать
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

import { Tabs, Typography } from 'antd';
import { ScheduleCalendar } from './schedules/schedule-calendar';
import { useGetEvents } from './api/use-get-events';
import { useUser } from '../../providers';
import { ROLE_ADMIN } from '../../config';
import { EventCreate } from './event-create';
import { ScheduleList } from './schedules/schedule-list';

export const EventsPageMain = () => {
  const { data: events } = useGetEvents();
  const { user } = useUser();

  return (
    <>
      <Typography.Title level={3}>Расписание</Typography.Title>
      {user && user.role === ROLE_ADMIN && <EventCreate />}
      <Tabs defaultActiveKey="calendar" centered>
        <Tabs.TabPane tab={<span>Календарь</span>} key="calendar">
          <ScheduleCalendar events={events} />
        </Tabs.TabPane>
        <Tabs.TabPane tab={<span>Список</span>} key="list">
          <ScheduleList />
        </Tabs.TabPane>
      </Tabs>
    </>
  );
};

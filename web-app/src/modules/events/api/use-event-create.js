import { useState } from 'react';
import { useSpin, useUser } from '../../../providers';
import axios from 'axios';
import { CREATE_EVENT_URL } from '../../../config';
import { useQueryClient } from 'react-query';

export const useEventCreate = () => {
  const [error, setError] = useState(false);
  const { spinStart, spinEnd } = useSpin();
  const { authHeaders } = useUser();
  const queryClient = useQueryClient();

  const onCreateEvent = async (body) => {
    const editBody = {
      name: body.name,
      description: body.description,
      start: body.showTime[0].toISOString(),
      end: body.showTime[1].toISOString(),
    };
    try {
      spinStart();
      const { data } = await axios.post(CREATE_EVENT_URL, editBody, {
        headers: { ...authHeaders },
      });
      queryClient.invalidateQueries('events');
    } catch {
      setError(true);
    }
    spinEnd();
  };

  return {
    onCreateEvent,
    error,
  };
};

import { useState } from 'react';
import { useSpin, useUser } from '../../../providers';
import axios from 'axios';
import { CANCEL_EVENT_URL } from '../../../config';
import { useQueryClient } from 'react-query';

export const useEventCancel = () => {
  const [error, setError] = useState(false);
  const { spinStart, spinEnd } = useSpin();
  const { authHeaders } = useUser();
  const queryClient = useQueryClient();

  const onCancelEvent = async (eventId) => {
    try {
      spinStart();
      const { data } = await axios.post(
        CANCEL_EVENT_URL,
        { eventId: eventId },
        {
          headers: { ...authHeaders },
        },
      );
      queryClient.invalidateQueries('events');
    } catch {
      setError(true);
    }
    spinEnd();
  };

  return {
    onCancelEvent,
    error,
  };
};

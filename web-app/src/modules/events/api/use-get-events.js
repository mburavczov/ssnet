import { useQuery } from 'react-query';
import { useUser } from '../../../providers';
import axios from 'axios';
import { GET_EVENTS_URL } from '../../../config';

export const useGetEvents = () => {
  const { authHeaders } = useUser();
  return useQuery(['events'], async () => {
    try {
      const dto = await axios.get(GET_EVENTS_URL, { headers: { ...authHeaders } });
      return dto.data;
    } catch {
      console.log('fatal error');
    }
  });
};

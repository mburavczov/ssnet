import { MainLayout, MenuForNoAuthUser } from '../../components/layout';
import { ImageEditorMain } from './image-editor-main';
import { Typography } from 'antd';

export const ImageEditorPage = () => {
  return (
    <MainLayout _sider={<MenuForNoAuthUser />}>
      <Typography.Title level={3}>Фоторедактор</Typography.Title>
      <ImageEditorMain />
    </MainLayout>
  );
};

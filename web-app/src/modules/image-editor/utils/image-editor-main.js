import { fabric } from 'fabric';

let canvas;
let polygonCrop = {
  points: [],
  lines: [],
  dots: [],
  isCropEvent: false,
};
let cropObject;

export class ImageEditor {
  constructor(canvasId) {
    canvas = new fabric.Canvas(canvasId);

    canvas.on('mouse:down', function (options) {
      if (!polygonCrop.isCropEvent) {
        return;
      }
      if (polygonCrop.lines.length !== 0) {
        polygonCrop.lines[polygonCrop.lines.length - 1].set({ stroke: '#4e4e4e', fill: '#4e4e4e' });
      }

      let pointer = canvas.getPointer(options.e);
      let isFirstDot = polygonCrop.points.length === 0;

      polygonCrop.dots.push(
        new fabric.Circle({
          left: pointer.x - 2,
          top: pointer.y - 2,
          radius: 4,
          fill: isFirstDot ? '#ff0000' : '#000',
          stroke: isFirstDot ? '#ff0000' : '#000',
          opacity: 0.3,
          strokeWidth: 1,
          hasBorders: false,
          hasControls: false,
        }),
      );

      let coordinates = polygonCrop.dots[polygonCrop.dots.length - 1].getCenterPoint();
      polygonCrop.lines.push(
        new fabric.Line(
          [coordinates.x - 1, coordinates.y - 1, coordinates.x - 1, coordinates.y - 1],
          {
            fill: '#e4e4e4',
            stroke: '#e4e4e4',
            strokeWidth: 1,
            selectable: false,
          },
        ),
      );
      canvas.add(polygonCrop.dots[polygonCrop.dots.length - 1]);
      canvas.add(polygonCrop.lines[polygonCrop.lines.length - 1]);
      polygonCrop.points.push({ x: coordinates.x, y: coordinates.y });
      canvas.renderAll();
    });

    canvas.on('mouse:move', function (options) {
      if (!polygonCrop.isCropEvent || polygonCrop.lines.length === 0) {
        return;
      }

      let pointer = canvas.getPointer(options.e);
      polygonCrop.lines[polygonCrop.lines.length - 1].set({ x2: pointer.x, y2: pointer.y });
      canvas.renderAll();
    });

    canvas.on('mouse:dblclick', () => {
      if (!polygonCrop.isCropEvent || polygonCrop.points.length === 0) {
        return;
      }

      this.endCrop();
    });
  }

  scaleImage(img) {
    let widthScale = canvas.width / (img.width + 50);
    let heightScale = canvas.height / (img.height + 50);

    if (widthScale > 1 || heightScale > 1) {
      return 1;
    }
    return widthScale > heightScale ? heightScale : widthScale;
  }

  add(url) {
    fabric.Image.fromURL(url, (img) => {
      img
        .set({
          left: 15,
          top: 15,
          right: 15,
          bottom: 15,
        })
        .scale(this.scaleImage(img));
      canvas.add(img);
      canvas.renderAll();
    });
  }

  save() {
    canvas.renderAll();
    const link = document.createElement('a');
    link.download = 'image.png';
    link.href = canvas.toDataURL({
      format: 'png',
      left: 0,
      top: 0,
      width: canvas.width,
      height: canvas.height,
    });
    link.click();
  }

  deleteGroupObjects(objects) {
    objects.forEach((object) => {
      canvas.remove(object);
    });
  }

  deleteCurrent() {
    canvas.remove(canvas.getActiveObject());
  }

  endCrop() {
    canvas.selection = true;
    polygonCrop.isCropEvent = false;

    canvas.getObjects().forEach((object) => {
      object.set({ selectable: true });
    });

    canvas.setActiveObject(cropObject);

    let polygon = new fabric.Polygon(polygonCrop.points, {
      fill: '#e4e4e4',
      strokeWidth: 1,
      stroke: '#4e4e4e',
    });

    cropObject.set({
      clipPath: new fabric.Polygon(polygonCrop.points, {
        left: -cropObject.left - cropObject.width / 2 + polygon.left,
        top: -cropObject.top - cropObject.height / 2 + polygon.top,
        offsetX: 'center',
        offsetY: 'center',
      }),
    });

    this.deleteGroupObjects(polygonCrop.lines);
    this.deleteGroupObjects(polygonCrop.dots);
    polygonCrop.lines = [];
    polygonCrop.dots = [];
    polygonCrop.points = [];

    canvas.renderAll();
  }

  toBack() {
    canvas.sendBackwards(canvas.getActiveObject());
    canvas.discardActiveObject();
    canvas.renderAll();
  }

  toFront() {
    canvas.bringForward(canvas.getActiveObject());
    canvas.discardActiveObject();
    canvas.renderAll();
  }

  startCrop() {
    cropObject = canvas.getActiveObject();
    if (!cropObject) {
      return;
    }
    canvas.selection = false;
    polygonCrop.isCropEvent = true;
    canvas.getObjects().forEach((object) => {
      object.set({ selectable: false });
    });
  }
}

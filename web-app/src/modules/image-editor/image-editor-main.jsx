import { useEffect, useRef } from 'react';
import { ImageEditor } from './utils/image-editor-main';
import { Button, Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import styled from 'styled-components';

export const ImageEditorMain = () => {
  const canvas = useRef(null);
  const editor = useRef({
    save: () => null,
    add: () => null,
    deleteCurrent: () => null,
    toFront: () => null,
    toBack: () => null,
  });
  useEffect(() => {
    editor.current = new ImageEditor('editor');
  }, []);

  function addImage(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      file.url = reader.result;
      editor.current.add(file.url);
    };
  }
  function save() {
    editor.current.save();
  }
  function deleteCurrent() {
    editor.current.deleteCurrent();
  }
  function startCrop() {
    editor.current.startCrop();
  }
  function toFront() {
    editor.current.toFront();
  }
  function toBack() {
    editor.current.toBack();
  }

  return (
    <div>
      <Container>
        <Upload showUploadList={false} beforeUpload={addImage}>
          <Button icon={<UploadOutlined />}>Загрузить файл</Button>
        </Upload>
        <Button onClick={save}>Сохранить</Button>
        <Button onClick={deleteCurrent}>Удалить объект</Button>
        <Button onClick={startCrop}>Обрезать</Button>
        <Button onClick={toFront}>На передний план</Button>
        <Button onClick={toBack}>На задний план</Button>
      </Container>
      <div>
        <canvas id="editor" width="700" height="700" ref={canvas} />
      </div>
    </div>
  );
};

const Container = styled.div`
  display: flex;
`;

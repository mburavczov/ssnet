import { Center, MainLayout } from '../../../components/layout';
import { SignInForm } from './sign-in-form';
import { useRedirectToMain } from '../use-redirect-to-main';

export const SignInPage = () => {
  useRedirectToMain();
  return (
    <MainLayout>
      <Center>
        <SignInForm />
      </Center>
    </MainLayout>
  );
};

import { useState } from 'react';
import axios from 'axios';
import { SIGN_IN_URL } from '../../../config';
import { useSpin, useUser } from '../../../providers';

export const useSignIn = () => {
  const [error, setError] = useState(false);
  const { spinStart, spinEnd } = useSpin();
  const { signIn } = useUser();

  const onSignIn = async (body) => {
    try {
      spinStart();
      const { data: dto } = await axios.post(SIGN_IN_URL, body);
      signIn(dto.token);
    } catch {
      setError(true);
      spinEnd();
    }
  };
  return {
    onSignIn,
    error,
  };
};

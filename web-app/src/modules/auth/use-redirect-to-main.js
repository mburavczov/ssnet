import { useEffect } from 'react';
import { useUser } from '../../providers';
import { useRouter } from 'next/router';

export const useRedirectToMain = () => {
  const { isAuth } = useUser();
  const router = useRouter();
  useEffect(() => {
    if (isAuth) {
      router.push('/');
    }
  }, []);
};

import { useState } from 'react';
import axios from 'axios';
import { SIGN_UP_URL } from '../../../config';
import { useSpin, useUser } from '../../../providers';

export const useSignUp = () => {
  const [error, setError] = useState(false);
  const { spinStart, spinEnd } = useSpin();
  const { signIn } = useUser();

  const onSignUp = async (body) => {
    try {
      spinStart();
      const { data: dto } = await axios.post(SIGN_UP_URL, body);
      signIn(dto.token);
    } catch {
      setError(true);
      spinEnd(true);
    }
  };
  return {
    onSignUp,
    error,
  };
};

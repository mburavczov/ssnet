import { Button, Form, Input, Typography } from 'antd';
import styled from 'styled-components';
import { useSignUp } from './use-sign-up';
import Link from 'next/link';
import { useSpin } from '../../../providers';

export const SignUpForm = () => {
  const { onSignUp, error } = useSignUp();
  const { spinStart } = useSpin();

  return (
    <Form name="auth" layout={'vertical'} onFinish={onSignUp}>
      <TitleContainer>
        <Typography.Title level={3}>РЕГИСТРАЦИЯ</Typography.Title>
      </TitleContainer>
      <Form.Item
        label="Имя"
        name="name"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено',
          },
        ]}
      >
        <Input placeholder={'Введите имя'} />
      </Form.Item>
      <Form.Item label="Фамилия" name="surname">
        <Input placeholder={'Введите фамилию'} />
      </Form.Item>
      <Form.Item
        label="E-mail"
        name="login"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено',
          },
          {
            type: 'email',
            message: 'Введите корректный e-mail',
          },
        ]}
      >
        <Input placeholder={'Введите e-mail'} />
      </Form.Item>
      <Form.Item label="Телефон" name="phone">
        <Input placeholder={'Введите номер телефона'} />
      </Form.Item>
      <Form.Item label="ВКонтакте" name="vk">
        <Input placeholder={'Ссылка на страницу ВК'} />
      </Form.Item>
      <Form.Item label="О себе" name="about">
        <Input.TextArea autoSize={true} placeholder="Расскажите о себе" />
      </Form.Item>
      <Form.Item
        label="Пароль"
        name="pass"
        rules={[
          {
            required: true,
            message: 'Необходимо ввести пароль',
          },
        ]}
      >
        <Input.Password placeholder={'Введите пароль'} />
      </Form.Item>
      <Form.Item key="submit">
        <Button style={{ width: '100%' }} type="primary" htmlType="submit">
          Зарегистрироваться
        </Button>
      </Form.Item>{' '}
      <Link href={'/sign-in'}>
        <Button type="link" onClick={spinStart} style={{ width: '100%' }}>
          Вход
        </Button>
      </Link>
    </Form>
  );
};

const TitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

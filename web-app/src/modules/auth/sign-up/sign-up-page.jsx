import { Center, MainLayout } from '../../../components/layout';
import { SignUpForm } from './sign-up-form';
import { useRedirectToMain } from '../use-redirect-to-main';

export const SignUpPage = () => {
  useRedirectToMain();
  return (
    <MainLayout>
      <Center>
        <SignUpForm />
      </Center>
    </MainLayout>
  );
};

import { Typography } from 'antd';
import { ChatsList } from './chats-list';
import { useGetChats } from '../api/use-get-chats';
import Link from 'next/link';

export const ChatListPageMain = () => {
  const { data, isLoading } = useGetChats();

  if ((!data || !data.chats) && isLoading) {
    return <Typography.Title level={3}>Чаты</Typography.Title>;
  }

  if (!data || !data.chats) {
    return (
      <>
        <Typography.Title level={3}>Чаты</Typography.Title>
        <Typography.Paragraph>
          Чтобы создать чат перейдите в раздел <Link href={'/users'}>Пользователи</Link>
        </Typography.Paragraph>
      </>
    );
  }

  return (
    <>
      <Typography.Title level={3}>Чаты</Typography.Title>
      <ChatsList chats={data.chats} />
    </>
  );
};

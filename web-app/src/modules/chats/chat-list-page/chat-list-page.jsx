import { MainLayout } from '../../../components/layout';
import { useRedirectSignIn } from '../../auth';
import { ChatListPageMain } from './chat-list-page-main';

export const ChatListPage = () => {
  useRedirectSignIn();
  return (
    <MainLayout _sider>
      <ChatListPageMain />
    </MainLayout>
  );
};

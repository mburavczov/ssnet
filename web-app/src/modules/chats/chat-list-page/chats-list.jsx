import { Button, Card } from 'antd';
import { useSpin } from '../../../providers';
import Link from 'next/link';

export const ChatsList = ({ chats }) => {
  if (!chats || chats.length === 0) return null;
  const { spinStart } = useSpin();

  return (
    <>
      {chats.map((chat) => (
        <Card
          key={chat.id}
          style={{ marginTop: 16 }}
          type="inner"
          title={chat.name}
          extra={
            <Button type="link" onClick={spinStart}>
              <Link href={`/chats/${chat.id}`}>Чат</Link>
            </Button>
          }
        >
          <b>Участники:</b>
          <ul>
            {chat.persons.map((person) => (
              <li key={person.name}>{person.name}</li>
            ))}
          </ul>
        </Card>
      ))}
    </>
  );
};

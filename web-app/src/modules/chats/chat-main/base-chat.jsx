import { useRouter } from 'next/router';
import { useGetChatMessages } from '../api/use-get-chat-messages';
import { Empty } from 'antd';
import { SendMessage } from './send-message';
import { ChatMessages } from './chat-messages';
import styled from 'styled-components';
import { DEVICES } from '../../../config';

export const BaseChat = () => {
  const { query } = useRouter();
  const { data, isLoading } = useGetChatMessages(query.id);

  if (!data) return null;
  if (isLoading && data && data.messages.length === 0) return null;

  if (data && data.messages.length === 0) {
    return (
      <>
        <Empty description={<span>Напишите первое сообщение</span>} />
        <SendMessage chatId={query.id} />
      </>
    );
  }

  return (
    <Container>
      <ChatMessages messages={data.messages} />
      <SendMessage chatId={query.id} />
    </Container>
  );
};

const Container = styled.div`
  max-width: 600px;
  @media ${DEVICES.tablet} {
    max-width: 320px;
  }
  @media ${DEVICES.mobile} {
    max-width: 230px;
  }
`;

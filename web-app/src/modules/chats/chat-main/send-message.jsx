import { Button, Input } from 'antd';
import { SendOutlined } from '@ant-design/icons';
import { useState } from 'react';
import { useSendMessage } from '../api/use-send-message';

export const SendMessage = ({ chatId }) => {
  const [message, setMessage] = useState('');
  const { onSendMessage } = useSendMessage();
  const sendMessage = async () => {
    if (message === '') return null;
    await onSendMessage(chatId, message);
    setMessage('');
  };

  return (
    <Input.Group style={{ marginTop: '20px' }} compact>
      <Input
        value={message}
        onChange={(event) => setMessage(event.target.value)}
        style={{ width: 'calc(100% - 46px)' }}
        placeholder={'Сообщение'}
        onKeyPress={async (event) => {
          if (event.key === 'Enter') {
            await sendMessage();
          }
        }}
      />
      <Button type="primary" onClick={sendMessage}>
        <SendOutlined />
      </Button>
    </Input.Group>
  );
};

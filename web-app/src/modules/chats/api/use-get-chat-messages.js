import { useUser } from '../../../providers';
import { useQuery } from 'react-query';
import axios from 'axios';
import { GET_CHAT_MESSAGE_URL } from '../../../config';

export const useGetChatMessages = (chatId) => {
  const { authHeaders } = useUser();
  return useQuery(
    ['chat-messages'],
    async () => {
      try {
        const dto = await axios.get(GET_CHAT_MESSAGE_URL, {
          headers: { ...authHeaders },
          params: { chatId },
        });
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {
      refetchInterval: 3000,
    },
  );
};

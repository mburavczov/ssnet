import { useState } from 'react';
import { useUser } from '../../../providers';
import { useQueryClient } from 'react-query';
import axios from 'axios';
import { CHAT_SEND_MESSAGE_URL } from '../../../config';

export const useSendMessage = () => {
  const [error, setError] = useState(false);
  const { authHeaders } = useUser();
  const queryClient = useQueryClient();

  const onSendMessage = async (chatId, message) => {
    try {
      await axios.post(
        CHAT_SEND_MESSAGE_URL,
        { chatId, text: message },
        {
          headers: { ...authHeaders },
        },
      );
      queryClient.invalidateQueries('chat-messages');
    } catch {
      setError(true);
    }
  };

  return {
    onSendMessage,
    error,
  };
};

import { useState } from 'react';
import { useSpin, useUser } from '../../../providers';
import { useQueryClient } from 'react-query';
import axios from 'axios';
import { CHAT_CREATE_URL } from '../../../config';

export const useCreateChat = () => {
  const [error, setError] = useState(false);
  const { spinStart, spinEnd } = useSpin();
  const { authHeaders } = useUser();
  const queryClient = useQueryClient();

  const onCreateChat = async (personIds, name) => {
    try {
      spinStart();
      await axios.post(
        CHAT_CREATE_URL,
        { personIds, name },
        {
          headers: { ...authHeaders },
        },
      );
      queryClient.invalidateQueries('chats');
    } catch {
      setError(true);
    }
    spinEnd();
  };

  return {
    onCreateChat,
    error,
  };
};

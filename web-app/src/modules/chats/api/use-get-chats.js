import { useUser } from '../../../providers';
import { useQuery } from 'react-query';
import axios from 'axios';
import { GET_CHATS_URL } from '../../../config';

export const useGetChats = () => {
  const { authHeaders } = useUser();
  return useQuery(['chats'], async () => {
    try {
      const dto = await axios.get(GET_CHATS_URL, { headers: { ...authHeaders } });
      return dto.data;
    } catch {
      console.log('fatal error');
    }
  });
};

import { MainLayout } from '../../../components/layout';
import { useRedirectSignIn } from '../../auth';
import { ChatMain } from '../chat-main/chat-main';

export const ChatPage = () => {
  useRedirectSignIn();
  return (
    <MainLayout _sider>
      <ChatMain />
    </MainLayout>
  );
};

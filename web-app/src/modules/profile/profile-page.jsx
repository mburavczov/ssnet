import { MainLayout } from '../../components/layout';
import { useRedirectSignIn } from '../auth';

export const ProfilePage = () => {
  useRedirectSignIn();
  return <MainLayout _sider>ProfilePage</MainLayout>;
};

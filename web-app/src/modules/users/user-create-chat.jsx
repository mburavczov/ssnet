import { useCreateChat } from '../chats';
import { Button, Form, Input } from 'antd';
import { useUser } from '../../providers';

export const UserCreateChat = ({ withUser }) => {
  const { onCreateChat } = useCreateChat();
  const [form] = Form.useForm();
  const { user } = useUser();

  return (
    <Form
      form={form}
      name="auth"
      layout={'vertical'}
      onFinish={(body) => {
        onCreateChat([withUser.id, user.id], body.name);
        form.resetFields();
      }}
    >
      <Form.Item
        label="Название чата"
        name="name"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено',
          },
        ]}
      >
        <Input placeholder={'Введите название чата'} />
      </Form.Item>
      <Form.Item style={{ marginTop: '-10px' }} key="submit">
        <Button type="primary" htmlType="submit">
          Создать чат
        </Button>
      </Form.Item>
    </Form>
  );
};

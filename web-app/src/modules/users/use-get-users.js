import { useUser } from '../../providers';
import { useQuery } from 'react-query';
import axios from 'axios';
import { GET_USERS_URL } from '../../config';

export const useGetUsers = () => {
  const { authHeaders } = useUser();
  return useQuery(['users'], async () => {
    try {
      const dto = await axios.get(GET_USERS_URL, { headers: { ...authHeaders } });
      return dto.data;
    } catch {
      console.log('fatal error');
    }
  });
};

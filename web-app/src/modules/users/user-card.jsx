import { Col, Row, Typography } from 'antd';
import { UserCreateChat } from './user-create-chat';

export const UserCard = ({ user }) => {
  return (
    <Row gutter={[16, 16]}>
      <Col>
        {user.login && (
          <Typography.Paragraph>
            E-mail: <a href={`mailto:${user.login}`}>{user.login}</a>
          </Typography.Paragraph>
        )}
        {user.vk && (
          <Typography.Paragraph>
            ВКонтакте:{' '}
            <a rel="noreferrer" target="_blank" href={user.vk}>
              {user.vk}
            </a>
          </Typography.Paragraph>
        )}
        {user.phone && <Typography.Paragraph>Номер телефона: {user.phone}</Typography.Paragraph>}
        {user.about && <Typography.Paragraph>О пользователе: {user.about}</Typography.Paragraph>}
      </Col>
      <Col>
        <UserCreateChat withUser={user} />
      </Col>
    </Row>
  );
};

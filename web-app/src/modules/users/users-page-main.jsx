import { Typography } from 'antd';
import { useGetUsers } from './use-get-users';
import { UsersList } from './users-list';

export const UsersPageMain = () => {
  const { data: users } = useGetUsers();

  return (
    <>
      <Typography.Title level={3}>Пользователи</Typography.Title>
      <UsersList users={users} />
    </>
  );
};

import { MainLayout } from '../../components/layout';
import { useRedirectSignIn } from '../auth';
import { UsersPageMain } from './users-page-main';

export const UsersPage = () => {
  useRedirectSignIn();
  return (
    <MainLayout _sider>
      <UsersPageMain />
    </MainLayout>
  );
};

import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import styled from 'styled-components';

export const UserCardHeader = ({ user }) => {
  const defaultAvatarStyle = { marginRight: '18px', cursor: 'pointer' };

  return (
    <Container>
      {user && user.avatarPath ? (
        <Avatar size={24} style={defaultAvatarStyle} src={user.avatarPath} />
      ) : (
        <Avatar size={24} style={defaultAvatarStyle} icon={<UserOutlined />} />
      )}
      {`${user.name} ${user.surname}`}
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  align-items: center;
`;

import { Collapse } from 'antd';
import { CaretRightOutlined } from '@ant-design/icons';
import { useUser } from '../../providers';
import { UserCard } from './user-card';
import { UserCardHeader } from './user-card-header';

export const UsersList = ({ users }) => {
  if (!users || users.length === 0) return null;
  const { user } = useUser();

  return (
    <Collapse
      bordered={false}
      expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
      className="site-collapse-custom-collapse"
    >
      {users
        .filter((item) => user.id !== item.id)
        .map((item) => (
          <Collapse.Panel
            header={<UserCardHeader user={item} />}
            key={item.id}
            className="site-collapse-custom-panel"
          >
            <UserCard user={item} />
          </Collapse.Panel>
        ))}
    </Collapse>
  );
};

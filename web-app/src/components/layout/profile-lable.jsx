import styled from 'styled-components';
import { useSpin, useUser } from '../../providers';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import Link from 'next/link';

export const ProfileLable = () => {
  const { isAuth } = useUser();
  const { spinStart } = useSpin();
  const { user } = useUser();
  const defaultAvatarStyle = { marginRight: '18px', cursor: 'pointer' };

  return (
    <Container>
      {isAuth ? (
        <>
          <Link href={'/profile'} passHref>
            {user && user.avatarPath ? (
              <Avatar style={defaultAvatarStyle} src={user.avatarPath} onClick={spinStart} />
            ) : (
              <Avatar style={defaultAvatarStyle} icon={<UserOutlined />} onClick={spinStart} />
            )}
          </Link>
          <Link href={'/profile'} passHref>
            <a className="ant-layout-sider-collapsed-a" onClick={spinStart}>
              {user && user.name}
            </a>
          </Link>
        </>
      ) : (
        <>
          <Link href={'/sign-in'} passHref>
            <Avatar style={defaultAvatarStyle} icon={<UserOutlined />} onClick={spinStart} />
          </Link>
          <Link href={'/sign-in'} passHref>
            <a className="ant-layout-sider-collapsed-a" onClick={spinStart}>
              Войти
            </a>
          </Link>
        </>
      )}
    </Container>
  );
};

const Container = styled.div`
  height: 60px;
  display: flex;
  align-items: center;
  color: white;
  padding-left: 24px;
`;

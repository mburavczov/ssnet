import { Menu } from 'antd';
import {
  ScheduleOutlined,
  HomeOutlined,
  BgColorsOutlined,
  ImportOutlined,
  WechatOutlined,
  TeamOutlined,
} from '@ant-design/icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useSpin, useUser } from '../../../providers';

export const MenuPersonalUser = () => {
  const router = useRouter();
  const { signOut } = useUser();
  const { spinStart } = useSpin();

  return (
    <Menu theme="dark" defaultSelectedKeys={router.pathname} mode="inline">
      <Menu.Item key="/" icon={<HomeOutlined />} onClick={spinStart}>
        <Link href={'/'}>Главная</Link>
      </Menu.Item>
      <Menu.Item key="/events" icon={<ScheduleOutlined />} onClick={spinStart}>
        <Link href={'/events'}>Расписание</Link>
      </Menu.Item>
      <Menu.Item key="/chats" icon={<WechatOutlined />} onClick={spinStart}>
        <Link href={'/chats'}>Чаты</Link>
      </Menu.Item>
      <Menu.Item key="/users" icon={<TeamOutlined />} onClick={spinStart}>
        <Link href={'/users'}>Пользователи</Link>
      </Menu.Item>
      <Menu.Item key="/image-editor" icon={<BgColorsOutlined />} onClick={spinStart}>
        <Link href={'/image-editor'}>Фоторедактор</Link>
      </Menu.Item>
      <Menu.Item
        key="5"
        icon={<ImportOutlined />}
        onClick={() => {
          if (router.pathname !== '/') {
            spinStart();
          }
          signOut();
        }}
      >
        Выйти
      </Menu.Item>
    </Menu>
  );
};

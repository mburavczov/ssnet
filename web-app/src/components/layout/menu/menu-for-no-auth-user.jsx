import { Menu } from 'antd';
import { BgColorsOutlined, HomeOutlined, UserAddOutlined } from '@ant-design/icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useSpin } from '../../../providers';

export const MenuForNoAuthUser = () => {
  const router = useRouter();
  const { spinStart } = useSpin();

  return (
    <Menu theme="dark" defaultSelectedKeys={router.pathname} mode="inline">
      <Menu.Item key="/" icon={<HomeOutlined />} onClick={spinStart}>
        <Link href={'/'}>Главная</Link>
      </Menu.Item>
      <Menu.Item key="/sign-up" icon={<UserAddOutlined />} onClick={spinStart}>
        <Link href={'/sign-up'}>Регистрация</Link>
      </Menu.Item>
      <Menu.Item key="/image-editor" icon={<BgColorsOutlined />} onClick={spinStart}>
        <Link href={'/image-editor'}>Фоторедактор</Link>
      </Menu.Item>
    </Menu>
  );
};

import styled from 'styled-components';

export const Center = ({ children, _maxWidth, _minusHeight, _alignItems = true }) => {
  return (
    <Container _alignItems={_alignItems} _minusHeight={_minusHeight}>
      <Content _maxWidth={_maxWidth}>{children}</Content>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: center;
  ${({ _alignItems }) => {
    if (_alignItems) return 'align-items: center;';
  }}

  min-height: calc(100vh - ${({ _minusHeight = 174 }) => _minusHeight}px);
`;

const Content = styled.div`
  max-width: ${({ _maxWidth = 400 }) => _maxWidth}px;
`;

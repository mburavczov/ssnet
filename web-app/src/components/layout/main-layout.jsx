import { Layout } from 'antd';
import { useState } from 'react';
import { Header } from './header';
import { Footer } from './footer';
import { useUser } from '../../providers';
import { MenuForNoAuthUser, MenuPersonalUser } from './menu';
import { ProfileLable } from './profile-lable';

export const MainLayout = ({ children, _sider = false }) => {
  const [collapsed, setCollapsed] = useState(true);
  const { isAuth } = useUser();

  return (
    <Layout style={{ minHeight: '100vh' }}>
      {_sider && (
        <Layout.Sider
          collapsible
          collapsed={collapsed}
          onCollapse={() => setCollapsed((prev) => !prev)}
        >
          <ProfileLable />
          {isAuth ? <MenuPersonalUser /> : <MenuForNoAuthUser />}
        </Layout.Sider>
      )}
      <Layout>
        <Layout.Header>
          <Header />
        </Layout.Header>
        <Layout.Content style={{ padding: '20px', backgroundColor: 'white' }}>
          {children}
        </Layout.Content>
        <Layout.Footer>
          <Footer />
        </Layout.Footer>
      </Layout>
    </Layout>
  );
};

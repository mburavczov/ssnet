import { Typography } from 'antd';
import styled from 'styled-components';

export const Footer = () => {
  return (
    <Container>
      <Typography.Text>Команда &quot;Феникс&quot;</Typography.Text>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: center;
`;

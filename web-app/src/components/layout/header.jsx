import { Typography } from 'antd';
import styled from 'styled-components';
import { DEVICES } from '../../config';

export const Header = () => {
  return (
    <Container>
      <Typography.Title level={3}>Student Social Network</Typography.Title>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  h3 {
    margin-bottom: 0;
    text-align: center;
    color: white;
  }
  @media ${DEVICES.mobile} {
    h3 {
      font-size: 18px;
    }
  }
`;

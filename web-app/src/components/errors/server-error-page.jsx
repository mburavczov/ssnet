import { Center, MainLayout } from '../layout';
import { Button, Result } from 'antd';
import Link from 'next/link';

export const ServerErrorPage = () => {
  return (
    <MainLayout>
      <Center>
        <Result
          status="500"
          title="500"
          subTitle="Ошибка на стороне сервера."
          extra={
            <Link href={'/'} passHref>
              <Button type="primary">Главная</Button>
            </Link>
          }
        />
      </Center>
    </MainLayout>
  );
};

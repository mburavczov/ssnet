import { HomePage } from '../modules/home';
import { UserProvider, useSpin } from '../providers';
import { HeadController } from '../components/layout/head-controller';

const Home = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="Главная" />
      <HomePage />
    </UserProvider>
  );
};

export default Home;

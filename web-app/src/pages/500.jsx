import { ServerErrorPage } from '../components/errors';
import { UserProvider, useSpin } from '../providers';
import { HeadController } from '../components/layout/head-controller';

const Error500 = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="500" />
      <ServerErrorPage />
    </UserProvider>
  );
};

export default Error500;

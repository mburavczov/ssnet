import { SignInPage } from '../modules/auth';
import { UserProvider, useSpin } from '../providers';
import { HeadController } from '../components/layout/head-controller';

const SignIn = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="Вход" />
      <SignInPage />
    </UserProvider>
  );
};

export default SignIn;

import { ImageEditorPage } from '../modules/image-editor';
import { UserProvider, useSpin } from '../providers';
import { HeadController } from '../components/layout/head-controller';

const ImageEditor = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="Фоторедактор" />
      <ImageEditorPage />
    </UserProvider>
  );
};

export default ImageEditor;

import { UsersPage } from '../modules/users';
import { UserProvider, useSpin } from '../providers';
import { HeadController } from '../components/layout/head-controller';

const Users = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="Пользователи" />
      <UsersPage />
    </UserProvider>
  );
};

export default Users;

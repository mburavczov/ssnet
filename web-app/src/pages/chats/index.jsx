import { ChatListPage } from '../../modules/chats';
import { UserProvider, useSpin } from '../../providers';
import { HeadController } from '../../components/layout/head-controller';

const Chats = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="Чаты" />
      <ChatListPage />
    </UserProvider>
  );
};

export default Chats;

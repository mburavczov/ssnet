import { ChatPage } from '../../modules/chats';
import { UserProvider, useSpin } from '../../providers';
import { HeadController } from '../../components/layout/head-controller';

const Chat = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="Чат" />
      <ChatPage />
    </UserProvider>
  );
};

export default Chat;

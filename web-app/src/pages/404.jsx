import { NotFoundPage } from '../components/errors';
import { UserProvider, useSpin } from '../providers';
import { HeadController } from '../components/layout/head-controller';

const Error404 = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="404" />
      <NotFoundPage />
    </UserProvider>
  );
};

export default Error404;

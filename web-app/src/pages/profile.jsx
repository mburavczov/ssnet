import { ProfilePage } from '../modules/profile';
import { UserProvider, useSpin } from '../providers';
import { HeadController } from '../components/layout/head-controller';

const Profile = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="Профиль" />
      <ProfilePage />
    </UserProvider>
  );
};
export default Profile;

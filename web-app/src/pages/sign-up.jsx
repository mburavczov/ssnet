import { SignUpPage } from '../modules/auth';
import { UserProvider, useSpin } from '../providers';
import { HeadController } from '../components/layout/head-controller';

const SignUp = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="Регистрация" />
      <SignUpPage />
    </UserProvider>
  );
};

export default SignUp;

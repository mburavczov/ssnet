import '../../public/styles/globals.css';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { SpinProvider } from '../providers';
import moment from 'moment';
import 'moment/locale/ru';
moment.locale('ru');

export default function MyApp({ Component, pageProps }) {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <SpinProvider>
        <Component {...pageProps} />
      </SpinProvider>
      <ReactQueryDevtools />
    </QueryClientProvider>
  );
}

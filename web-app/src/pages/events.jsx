import { EventsPage } from '../modules/events';
import { UserProvider, useSpin } from '../providers';
import { HeadController } from '../components/layout/head-controller';

const Events = () => {
  const { spinAfterLoad } = useSpin();
  spinAfterLoad();
  return (
    <UserProvider>
      <HeadController title="Расписание" />
      <EventsPage />
    </UserProvider>
  );
};
export default Events;

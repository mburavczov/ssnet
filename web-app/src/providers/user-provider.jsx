import React, { useContext } from 'react';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';
import { useQuery } from 'react-query';
import { PROFILE_INFO_URL } from '../config';
import axios from 'axios';
const UserContext = React.createContext();

export const useUser = () => {
  return useContext(UserContext);
};

export const UserProvider = ({ children }) => {
  const [cookies, setCookie, removeCookie] = useCookies();
  const router = useRouter();

  const isAuth = !!cookies.authJwtToken;
  const signIn = (token) => {
    setCookie('authJwtToken', token);
    router.push('/');
  };
  const signOut = () => {
    removeCookie('authJwtToken');
    router.push('/');
  };
  const authHeaders = {
    Authorization: `Bearer ${cookies.authJwtToken}`,
  };
  const { data: user } = useQuery(
    ['user'],
    async () => {
      try {
        if (!isAuth) return {};
        const dto = await axios.get(PROFILE_INFO_URL, { headers: { ...authHeaders } });
        return dto.data;
      } catch {
        console.log('Fatal error');
      }
    },
    { refetchOnWindowFocus: false, refetchOnMount: false },
  );

  return (
    <UserContext.Provider value={{ signIn, isAuth, signOut, authHeaders, user }}>
      {children}
    </UserContext.Provider>
  );
};

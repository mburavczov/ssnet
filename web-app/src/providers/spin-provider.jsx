import React, { useContext, useEffect, useState } from 'react';
import { Spin } from 'antd';
import { useIsFetching } from 'react-query';
import { useRouter } from 'next/router';

const SpinContext = React.createContext();

export const useSpin = () => {
  return useContext(SpinContext);
};

export const SpinProvider = ({ children }) => {
  const [isSpin, setSpin] = useState(false);
  const isFetching = useIsFetching();
  const router = useRouter();

  const spinStart = () => setSpin(true);
  const spinEnd = () => setSpin(false);
  const spinAfterLoad = () =>
    useEffect(() => {
      setSpin(false);
    }, []);

  return (
    <SpinContext.Provider value={{ spinStart, spinEnd, spinAfterLoad }}>
      <Spin spinning={isSpin || (!!isFetching && router.route !== '/chats/[id]')} size="large">
        {children}
      </Spin>
    </SpinContext.Provider>
  );
};

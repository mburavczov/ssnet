# Student Social Network

Сервис социальной сети для учащихся, который позволяет создавать и 
редактировать свой профиль, просматривать профиль других участников, просматривать 
и создавать расписание занятий, общаться в чате. Также есть возможность загружать 
и редактировать фотографии. Реализовано разграничение прав доступа на пользователя 
и админа. Сервис реализован в виде единого API, web и android приложения.

### Кейсы
- Соц сеть учащихся - Rubicone (кейс 18)
- Редактор изображений - Smartech (кейс 11)
- Веб сервис и мобильное приложение в связке - DevExpress (кейс 6)


## Стек технологий

Бэкенд написан на [ASP.NET Core 5](https://dotnet.microsoft.com/apps/aspnet).
Фронтенд на [React](https://reactjs.org/) с использованием фреймворков
[Next.js](https://nextjs.org/) и [Ant Design](https://ant.design/), а также библиотеки 
[React Query](https://react-query.tanstack.com/). Редактор изображений основан на 
библиотеки [Fabric.js](http://fabricjs.com/). Android приложение на написано на Kotlin.


Настроен CI/CD, API и web-сервис запускаются внутри [Docker](https://www.docker.com/)
контейнера и разворачиваются на [Heroku](https://www.heroku.com/).


## Демо

Видео работы и скриншоты: https://drive.google.com/drive/folders/1Ko73so8w5J-2jo1gwhi-ejPjCnkglnYX

API: https://phoenix-art-api.herokuapp.com/swagger/index.html  
WEB: https://phoenix-art.herokuapp.com/

Доступы (логин:пароль):
- user1@gmail.com:1?x6aeLt
- mail1@gmail.com:12345


## Развёртывание

Сервисы запускаются внутри докер контейнеров.
Команды нужно выпонять в корневой директории проекта.

Запуск API
```bash
docker build -f api/Dockerfile -t ssnet_api .
docker run -p 8080:8080 -e "DROPBOX_TOKEN=<TOKEN>" -e "PORT=8080" -it ssnet_api
```
Проверить запуск можно открыв swagger http://localhost:8080/swagger/index.html

Запуск web приложения
```bash
docker build -f web-app/Dockerfile -t ssnet_web .
docker run -p 8081:8081 -e "NEXTAUTH_URL='localhost:8081'" -e "PORT=8081" -it ssnet_web
```
Приложение будет доступно по адресу http://localhost:8081/
